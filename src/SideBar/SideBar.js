import React from "react";
import { AppRegistry, Image, StatusBar } from "react-native";
import {
    Button,
    Text,
    Container,
    List,
    ListItem,
    Content,
    Footer,
    FooterTab,
    Icon
} from "native-base";

export default class SideBar extends React.Component {
  render() {
    return (
        <Container>
            <Content>                                
                <Image
                    source={{
                        uri: "https://github.com/GeekyAnts/NativeBase-KitchenSink/raw/react-navigation/img/drawer-cover.png"
                    }}
                    style={{
                        height: 120,
                        alignSelf: "stretch",
                        justifyContent: "center",
                        alignItems: "center"
                    }}
                />                
                <Image
                    square
                    style={{ height: 80, width: 70 }}
                    source={{
                        uri: "https://github.com/GeekyAnts/NativeBase-KitchenSink/raw/react-navigation/img/logo.png"
                    }}
                />
                <Button 
                    full 
                    iconRight 
                    onPress={() => this.props.navigation.navigate("Perfil")}
                >
                    <Text>Editar Perfil</Text>
                    <Icon name='md-create' />
                </Button>
                <List>
                    <ListItem
                        button
                        onPress={() => this.props.navigation.navigate("Início")}
                    >
                        <Icon ios='ios-home' android='md-home' style={{fontSize: 20, color: 'black'}} />
                        <Text> Início</Text>                        
                    </ListItem>   
                </List>
                <List>
                    <ListItem
                        button
                        onPress={() => this.props.navigation.navigate("Treinos")}
                    >                        
                        <Icon ios='ios-walk' android='md-walk' style={{fontSize: 20, color: 'black'}} />
                        <Text> Meus Treinos</Text>                        
                    </ListItem>   
                </List>
                {/* 
                TODO: Incluir uma ferramenta de comunicação entre o Personal e o Aluno
                <List>
                    <ListItem
                        button
                        onPress={() => this.props.navigation.navigate("Mensagem")}
                    >
                        <Text>Mensagem</Text>
                    </ListItem>   
                </List> 
                */}           
                <List>
                    <ListItem
                        button
                        //TODO: onPress={() => CHAMA O LOGOUT}
                    >
                        <Icon ios='ios-exit' android='md-exit' style={{fontSize: 20, color: 'black'}} />
                        <Text> Sair</Text>
                    </ListItem>
                </List>                    
            </Content>
        </Container>
    );
  }
}
