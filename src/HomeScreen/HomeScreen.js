import React from "react";
import { StatusBar } from "react-native";
import {
  Button,
  Text,
  Container,
  Card,
  CardItem,
  Body,
  Content,
  Header,
  Title,
  Subtitle,
  Left,
  Icon,
  Right
} from "native-base";
import { StackNavigator } from "react-navigation";

export default class HomeScreen extends React.Component {
  
    constructor(props){
        super(props);
        this.state = {
            isLoading: true,
        };
        this.dataArray = [];
    }
    
    //WS call
    componentDidMount() {
        return fetch('https://plataforma-de-treino.herokuapp.com/trainings/')
        .then((response) => response.json())
        .then((responseJson) => {
            this.dataArray = responseJson;
            this.setState({
                isLoading: false
            }, 
            function() {
                // do something with new state
                //alert(this.dataArray[1].name);
            });
        })
        .catch((error) => {
            console.error(error);
        });
    }
  
    render() {
        const { navigate } = this.props.navigation;
        return (
            <Container>
                <Header
                    androidStatusBarColor="#000000"            
                    style={{ marginTop: 24 }}
                >  
                    <Left>
                        <Button
                            transparent
                            onPress={() => this.props.navigation.navigate("DrawerOpen")}>
                            <Icon name="menu" />
                        </Button>
                    </Left>
                    <Body
                        style={{
                            flex: 3
                        }}
                    >
                        <Title>Plataforma de Treino</Title>
                    </Body>
                <Right />
                </Header>
                <Content padder>
                    <Text
                        style={{
                            fontSize: 20,
                            fontWeight: "bold",
                            textAlign:"center"
                        }}
                    >
                        Treinos disponíveis
                    </Text>
                    <Card 
                        dataArray={this.dataArray} 
                        renderRow={(item) =>
                            <CardItem
                                button
                                onPress={() => navigate("TrainingDetail",{trainingId: item.id}) }
                            >
                                <Body>
                                    <Text>{item.name}</Text>
                                    <Text note>{item.description}</Text>
                                </Body>
                                <Right>
                                    <Icon name="arrow-forward" />
                                </Right>
                            </CardItem>
                        } 
                    />                   
                </Content>
            </Container>
    );
  }
}
