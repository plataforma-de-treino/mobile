import React from "react";
import HomeScreen from "./HomeScreen.js";
import TrainingScreenNavigator from "../TrainingScreen/index.js";
import SideBar from "../SideBar/SideBar.js";
import { DrawerNavigator } from "react-navigation";
const HomeScreenRouter = DrawerNavigator(
    {
        Início: { screen: HomeScreen },
        Treinos: {screen: TrainingScreenNavigator}        
    },
    {
        contentComponent: props => <SideBar {...props} />
    }
);
export default HomeScreenRouter;
