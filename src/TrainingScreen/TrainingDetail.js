
import React from "react";
import { AppRegistry, Alert } from "react-native";
import {
    Text,
    Container,
    List, 
    ListItem,
    Card,
    CardItem,
    Body,
    Content,
    Header,
    Left,
    Right,
    Icon,
    Title,
    Button,
    H1
} from "native-base";
import { StackNavigator } from "react-navigation";

export default class TrainingDetail extends React.Component {
    
    constructor(props){
        super(props);
        this.state = {
            isLoading: true,
        };
        this.dataTraining = [];
        this.dataArray = [];
    }
    
    //WS call
    componentDidMount() {
        const { state } = this.props.navigation;
        return fetch("https://plataforma-de-treino.herokuapp.com/trainings/" + state.params.trainingId + "/")
        .then((response) => response.json())
        .then((responseJson) => {
            this.dataTraining = responseJson;
            this.setState({
                isLoading: false
            }, 
            function() {
                //do something with new state
                //alert(this.dataArray.name);
                this.dataTraining.routines.forEach(function(element) {
                    return fetch("https://plataforma-de-treino.herokuapp.com/routines/" + element + "/")
                    .then((response) => response.json())
                    .then((responseJson) => {
                        this.dataArray.push(responseJson);
                        this.setState({
                            isLoading: false
                        }, 
                        function() {
                            //do something with new state
                            //alert(this.dataArray.name);
                        });
                    })
                    .catch((error) => {
                        console.error(error);
                    });
                }, this);
            });
        })
        .catch((error) => {
            console.error(error);
        });
    }

    //Screen definition
    render() {
        const { navigate } = this.props.navigation;
        return (
            <Container>
                <Content padder>
                    <List>
                        <ListItem itemDivider>
                            <Body>
                                <Text>{this.dataTraining.name}</Text>
                                <Text note>{this.dataTraining.description}</Text>
                            </Body>
                        </ListItem>
                    </List>
                    <List 
                    dataArray={this.dataArray} 
                    renderRow={(item) =>
                        <ListItem
                        button
                        onPress={() => navigate("Routine",{routineId: item.id}) }
                        >
                            <Body>
                                <Text>{item.name}</Text>
                            </Body>
                            <Right>
                                <Icon name="arrow-forward" />
                            </Right>
                        </ListItem>
                    } />
                </Content>
            </Container>
        );
    }

}

//Back Arrow Navigation
TrainingDetail.navigationOptions = ({ navigation }) => ({
   
    header: (
        <Header
            androidStatusBarColor="#000000"            
            style={{ marginTop: 24 }}
        >
            <Left>
                <Button transparent onPress={() => navigation.goBack()}>
                    <Icon name="arrow-back" />
                </Button>
            </Left>
            <Body
                style={{
                    flex: 3
                }}
            >
                <Title>Meus Treinos</Title>
            </Body>
            <Right />
        </Header>
    )
    
});
