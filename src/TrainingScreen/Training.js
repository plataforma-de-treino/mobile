
import React from "react";
import { AppRegistry, Alert } from "react-native";
import {
    Text,
    Container,
    List, 
    ListItem,
    Card,
    CardItem,
    Body,
    Content,
    Header,
    Left,
    Right,
    Icon,
    Title,
    Button,
    H1
} from "native-base";
import { StackNavigator } from "react-navigation";

export default class Training extends React.Component {
    
    constructor(props){
        super(props);
        this.state = {
            isLoading: true,
        };
        this.dataArray = [];
    }
    
    //WS call
    componentDidMount() {
        return fetch('https://plataforma-de-treino.herokuapp.com/trainings/')
        .then((response) => response.json())
        .then((responseJson) => {
            this.dataArray = responseJson;
            this.setState({
                isLoading: false
            }, 
            function() {
                // do something with new state
                //alert(this.dataArray[1].name);
            });
        })
        .catch((error) => {
            console.error(error);
        });
    }

    //Screen definition
    render() {
        const { navigate } = this.props.navigation;
        return (
            <Container>
                <Content padder>
                    <List 
                    dataArray={this.dataArray} 
                    renderRow={(item) =>
                        <ListItem
                        button
                        onPress={() => navigate("TrainingDetail",{trainingId: item.id}) }
                        >
                            <Body>
                                <Text>{item.name}</Text>
                                <Text note>{item.description}</Text>
                            </Body>
                            <Right>
                                <Icon name="arrow-forward" />
                            </Right>
                        </ListItem>
                    } />
                </Content>
            </Container>
        );
    }

}

//Drawer
Training.navigationOptions = ({ navigation }) => ({
    header: (
        <Header
            androidStatusBarColor="#000000"            
            style={{ marginTop: 24 }}
        >
            <Left>
                <Button transparent onPress={() => navigation.navigate("DrawerOpen")}>
                <Icon name="menu" />
                </Button>
            </Left>
            <Body
                style={{
                    flex: 3
                }}
            >
                <Title>Meus Treinos</Title>
            </Body>
            <Right />
        </Header>
    )
});
