import React from "react";
import { 
    ActivityIndicator,
    AppRegistry, 
    Alert,
    View 
} from "react-native";
import {
    Text,
    Container,
    List, 
    ListItem,
    Card,
    CardItem,
    Body,
    Content,
    Header,
    Left,
    Right,
    Icon,
    Title,
    Button,
    H1
} from "native-base";
import { StackNavigator } from "react-navigation";
import { Video } from 'expo';
import VideoPlayer from '@expo/videoplayer';

export default class Routine extends React.Component {
    
    constructor(props){
        super(props);
        this.state = {
            isLoading: true,
        };
        this.dataRoutine = [];
        this.dataArray = [];
    }
    
    ///WS call
    componentDidMount() {
        const { state } = this.props.navigation;
        return fetch("https://plataforma-de-treino.herokuapp.com/routines/" + state.params.routineId + "/")
        .then((response) => response.json())
        .then((responseJson) => {
            this.dataRoutine = responseJson;
            this.setState({
                isLoading: false
            }, 
            function() {
                //do something with new state
                //alert(this.dataArray.name)                
                return fetch("https://plataforma-de-treino.herokuapp.com/exercises/" + this.dataRoutine.exercise + "/")
                .then((response) => response.json())
                .then((responseJson) => {
                    this.dataArray.push(responseJson);
                    this.setState({
                        isLoading: false
                    }, 
                    function() {
                        //do something with new state
                        //alert(this.dataArray.name);
                    });
                })
                .catch((error) => {
                    console.error(error);
                });                
            });
        })
        .catch((error) => {
            console.error(error);
        });
    }

    //Screen definition
    render() {
        const { navigate } = this.props.navigation;
        return (
            <Container>
                <Content padder>
                    <List>
                        <ListItem itemDivider>
                            <Body>
                                <Text>{this.dataRoutine.name}</Text>                                
                            </Body>
                        </ListItem>
                    </List>
                    <List 
                    dataArray={this.dataArray} 
                    renderRow={(item) =>
                        <ListItem>     
                            <Body>     
                                <View style={{ width: 325, height: 200 }}>                                                                                     
                                    <VideoPlayer
                                        videoProps={{
                                            shouldPlay: true,
                                            resizeMode: Video.RESIZE_MODE_CONTAIN,
                                            source: {
                                            uri: item.video,
                                            },
                                        }}
                                        isPortrait={true}
                                        playFromPositionMillis={0}                                    
                                    />   
                                </View>                      
                                <CardItem>
                                    <Body>
                                        <Text>{item.description}</Text> 
                                    </Body>
                                </CardItem>                                                                                                               
                            </Body>
                        </ListItem>                        
                    } />
                    <List>
                        <ListItem>
                            <Body>
                                <Text>Repetições: {this.dataRoutine.repetition}</Text>
                                <Text>Peso: {this.dataRoutine.weight}</Text>
                                <Text note>Descanso: {this.dataRoutine.rest}</Text>
                                <Text note>Limite de Tempo: {this.dataRoutine.duration}</Text>                                
                            </Body>
                        </ListItem>
                    </List>
                </Content>
            </Container>
        );
    }

}

//Back Arrow Navigation
Routine.navigationOptions = ({ navigation }) => ({
    header: (
        <Header
            androidStatusBarColor="#000000"            
            style={{ marginTop: 24 }}
        >
            <Left>
                <Button transparent onPress={() => navigation.goBack()}>
                    <Icon name="arrow-back" />
                </Button>
            </Left>
            <Body
                style={{
                    flex: 3
                }}
            >
                <Title>Série</Title>
            </Body>
            <Right />
        </Header>
    ) 
});
