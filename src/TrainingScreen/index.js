
import React from "react";
import Training from "./Training.js";
import TrainingDetail from "./TrainingDetail.js";
import Routine from "./Routine.js";
import { StackNavigator } from "react-navigation";
export default (
    DrawNav = StackNavigator(
        {
            Training: { screen: Training },
            TrainingDetail: { screen: TrainingDetail },
            Routine: { screen: Routine }
        }
    ) 
);